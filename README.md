## What

An ansible playbook (`jenkins.yaml`) to install Docker (with `buildx` and `compose` plugins) to support Jenkins on a "fresh" Debian based system.

Also a secondary playbook (`uninstall.yaml`) to clean things up.


## How it works

- Install some `apt` bits in order to reliably add Docker's `apt` repo, and from there install `docker-ce` + `buildx` + `compose` w/`containerd` 
- Generate an ephemeral `ed25519` ssh keypair for Jenkins and the `ssh-agent` worker to talk to each other within their docker network and render 
into templates locally
- Copy templates to remote system, and update the external IP for `jenkins_casc.yaml`, also install a weekly cronjob to run `docker prune` images 
and system caches
- Builds Jenkins controller + agent containers ensuring they're fresh before running `docker compose up` to start Jenkins which launches to the initial Admin User creation wizard, and then into a stood up Jenkins with some simple **Jobs** templated below
- Cleanup locally rendered templates and ephemeral keypair

## Templates

- `compose.j2` ~ renders to `compose.yaml` for Docker compose
- `Dockerfile.j2` ~ renders to `Dockerfile` for Jenkins controller and agent
- `jenkins_casc.j2` ~ renders to `jenkins_casc.yaml` for Jenkins **C**onfiguration as **C**ode plugin


# Dockerfiles

Very simple reuse of upstream images, using `jenkins-plugin-cli` + `files/jenkins_plugins.txt` preinstall plugins into the container
These plugins were Jenkin's own "recommended" list with `GitHub` removed, [Gerrit-Trigger](https://plugins.jenkins.io/gerrit-trigger/) and [ssh-steps](https://plugins.jenkins.io/ssh-steps/) added
Second stage in the Dockerfile adds `docker` cli to the `ssh-agent` worker container allowing it to interact with local Docker daemon via `docker.sock` 
which is mounted inside the container via `compose.yaml`.
There are two used in the `files/jobs` pipelines which are written to a file.  It was easiest way to have them available for Pipeline testing vs a more 
ideal solution like being in a repo that is cloned into the Pipeline's workspace.


## Jenkins CasC

The [Jenkins CasC plugin](https://plugins.jenkins.io/configuration-as-code/) allows for using YAML formatted configuration file allowing for repeatable 
deployments.  It also adds an item to `Manage Jenkins` to allow for reloading, downloading and viewing the current configuration so changes made can be 
added to the existing `jenkins_casc.j2` template.  Many options were omitted from the YAML cfg so just use Jenkins own defaults.


## Jobs

- `docker-build-calyxos.org` ~ clones calyx.org repo, runs Jekyll container to render site, then bakes into Alpine based nginx container
- `docker-build-jekyll` ~ builds a Debian based Jekyll container, basically just `apt install -y ruby-jekyll*` (needed for `docker-build-calyxos.org` pipeline)
- `docker-gradle` ~ simple example using official Gradle image with version as a build-parameter
- `docker-maven` ~ simple example using official Maven image with version as a build-parameter
- `hello-world` ~ Jenkins own `Hello World` example Pipeline


## Variables

- `ansible_user` ~ user for ansible to connect as
- `docker_cron_path` ~ path where script to run `docker image prune && docker system prune` regularly
- `jenkins_admin_email` ~ email for Jenkins System Adminstration (`Manage Jenkins` > `System`)
- `jenkins_docker_gid` ~ group id for Docker, needed for Jenkins user inside `ssh-agent` container to access `docker.sock`
- `jenkins_docker_tag_version` ~ docker tag templated into Jenkins Dockerfile
- `jenkins_external_ip` ~ set the external IP address for Jenkins (defaults to ansible's `default_ipv4.address` if undefined)
- `jenkins_gerrit_email` ~ email of user for Gerrit Trigger connection
- `jenkins_gerrit_name` ~ hostname for Gerrit Trigger connection 
- `jenkins_gerrit_privkey_path` ~ path inside Jenkins to Gerrit Trigger connection's ssh privkey
- `jenkins_gerrit_privkey_passwd` ~ passwd (if any) for Gerrit Trigger ssh privkey
- `jenkins_gerrit_ssh_port` ~ port for Gerrit Trigger ssh connection
- `jenkins_gerrit_user` ~ user for Gerrit Trigger connection
- `jenkins_java_opts` ~ Java options for Jenkins startup (skips full setup wizard)
- `jenkins_remote_path` ~ path to where to install Jenkins
- `gerrit_ssh_privkey` ~ ssh privkey for Gerrit Trigger


## Cavaets
- groupid for `docker` differs between `debian` (host) and `alpine` (container) so need to change docker's gid in worker container to match the host
- Jenkins CasC relying on YAML can be somewhat quirky about ssh key formatting because YAML
- `ssh-steps` plugin is useful for connecting to remote system as part of a CI/CD pipeline but also automating some DevOps tasks, but it seems to use
a Java `ssh` implementation instead of openssh and only seems to work with `rsa` keys
- locally rendering the ephemeral key-pair, needed to do it that way to simplify templating